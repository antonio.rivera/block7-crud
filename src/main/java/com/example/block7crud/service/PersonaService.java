package com.example.block7crud.service;

import com.example.block7crud.model.Persona;

import java.util.List;
import java.util.Optional;

public interface PersonaService {
    Persona addPersona(Persona persona);

    Optional<Persona> modifyPersonaById(Long id, Persona persona);

    Optional<Persona> deletePersona(Long id);

    Optional<Persona> findPersonById(Long id);

    Persona findByNombre(String nombre);

    List<Persona> listAllPersona();
}
