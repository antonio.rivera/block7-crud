package com.example.block7crud.service.impl;

import com.example.block7crud.model.Persona;
import com.example.block7crud.repository.PersonaRepository;
import com.example.block7crud.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;

import java.util.Optional;

@Service
@RequiredArgsConstructor
//@NoArgsConstructor @AllArgsConstructor
public class PersonaServiceImpl implements PersonaService {



    private final PersonaRepository personaRepository;

    @Override
    public Persona addPersona(Persona persona) {
        personaRepository.save(persona);
        return persona;
    }


    @Override
    public Optional<Persona> modifyPersonaById(Long id, Persona persona) {
        Optional<Persona> personaModificada=personaRepository.findById(id);
        if (personaRepository.findById(id).isPresent()){
            if(!persona.getNombre().isEmpty()){
                personaModificada.get().setNombre(persona.getNombre());
            }
            if (!persona.getEdad().isEmpty()){
                personaModificada.get().setEdad(persona.getEdad());
            }
            if (!persona.getPoblacion().isEmpty()){
                personaModificada.get().setPoblacion(persona.getPoblacion());
            }
            personaRepository.save(personaModificada.get());
        }
        return personaModificada;
    }

    @Override
    public Optional<Persona> deletePersona(Long id) {
        Optional<Persona> personaEliminada = personaRepository.findById(id);
        if (personaRepository.findById(id).isPresent()){
            personaRepository.deleteById(id);
        }
        return personaEliminada;
    }

    @Override
    public Optional<Persona> findPersonById(Long id) {
        return personaRepository.findById(id);
    }

    @Override
    public Persona findByNombre(String nombre) {
        return personaRepository.findByNombre(nombre);
    }


    @Override
    public List<Persona> listAllPersona() {
        return personaRepository.findAll();
    }
}
