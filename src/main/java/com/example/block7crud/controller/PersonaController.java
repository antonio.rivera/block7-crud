package com.example.block7crud.controller;


import com.example.block7crud.model.Persona;
import com.example.block7crud.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/persona")
public class PersonaController {
    @Autowired
    private PersonaService personaService;

    @PostMapping
    public ResponseEntity<Persona> addPersona(@RequestBody Persona persona){
        return ResponseEntity.ok(personaService.addPersona(persona));
    }


    @PutMapping("/{id}")
    public ResponseEntity<Optional<Persona>> modifyPersonaById(@PathVariable Long id, @RequestBody Persona persona){
        return ResponseEntity.ok(personaService.modifyPersonaById(id,persona));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Optional<Persona>> deletePersona(@PathVariable Long id){
        return ResponseEntity.ok(personaService.deletePersona(id));
    }

    @GetMapping("/porId/{id}")
    public ResponseEntity<Optional<Persona>> findPersonById(@PathVariable Long id){
        Optional<Persona> persona = personaService.findPersonById(id);
        return ResponseEntity.ok(persona);
    }


    @GetMapping("/porNombre/{nombre}")
    public ResponseEntity<Persona> findByNombre(@PathVariable String nombre){
        return ResponseEntity.ok(personaService.findByNombre(nombre));
    }

    @GetMapping
    public ResponseEntity<List<Persona>> listAllPersona(){
        return ResponseEntity.ok(personaService.listAllPersona());
    }



}
