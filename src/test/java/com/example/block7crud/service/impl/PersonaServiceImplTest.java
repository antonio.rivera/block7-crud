package com.example.block7crud.service.impl;

import com.example.block7crud.model.Persona;
import com.example.block7crud.repository.PersonaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PersonaServiceImplTest {

    @Mock
    private PersonaRepository personaRepository;

    @InjectMocks
    private PersonaServiceImpl personaService;


    @Test
    void addPersona() {
        Persona personaSimulada = new Persona(1L,"Antonio1","23","jodar1");
        Persona personaEsperada = new Persona(1L,"Antonio1","23","jodar1");
        Mockito.when(personaRepository.save(personaSimulada)).thenReturn(personaSimulada);
        final Persona resultado = personaService.addPersona(personaSimulada);
        assertEquals(resultado,personaEsperada);
        Mockito.verify(personaRepository).save(personaSimulada);
    }

    @Test
    void modifyPersonaById() {
        Persona personaSimulada = new Persona(1L,"Antonio1","23","jodar1");
        Persona personaModificada = new Persona(2L,"Antonio2","232","jodar2");
        Persona personaEsperada = new Persona(2L,"Antonio2","232","jodar2");
        Mockito.when(personaService.modifyPersonaById(1L,personaModificada)).thenReturn(Optional.of(personaModificada));
        final Optional<Persona> resultado = personaService.modifyPersonaById(1L,personaModificada);
        assertEquals(resultado,Optional.of(personaEsperada));
    }

    @Test
    void deletePersona() {
        Persona personaSimulada = new Persona(1L,"Antonio1","23","jodar1");
        Persona personaEsperada = new Persona(1L,"Antonio1","23","jodar1");
        Mockito.when(personaService.deletePersona(1L)).thenReturn(Optional.of(personaSimulada));
        final Optional<Persona> resultado = personaService.deletePersona(1L);
        assertEquals(resultado,Optional.of(personaEsperada));
        Mockito.verify(personaRepository).deleteById(1L);
    }

    @Test
    void findPersonById() {
        Persona personaSimulada = new Persona(1L,"Antonio1","23","jodar1");
        Optional<Persona> personaEsperada = Optional.of(new Persona(1L, "Antonio1", "23", "jodar1"));
        Mockito.when(personaService.findPersonById(1L)).thenReturn(Optional.of(personaSimulada));

        final Optional<Persona> resultado = personaService.findPersonById(1L);
        //final Persona persona = personaService.findPersonById(1L);
        assertEquals(personaEsperada,resultado);
        Mockito.verify(personaRepository).findById(1L);
    }

    @Test
    void findByNombre() {
        Persona personaSimulada = new Persona(1L,"Antonio1","23","jodar1");
        Persona personaEsperada = new Persona(1L, "Antonio1", "23", "jodar1");
        Mockito.when(personaService.findByNombre("Antonio1")).thenReturn(personaSimulada);
        final Persona resultado = personaService.findByNombre("Antonio1");
        assertEquals(personaEsperada,resultado);
        Mockito.verify(personaRepository).findByNombre("Antonio1");
    }

    @Test
    void listAllPersona() {
        List<Persona> personaListSimulada = new ArrayList<>();
        personaListSimulada.add((new Persona(1L,"Antonio1","23","jodar1")));
        personaListSimulada.add((new Persona(2L,"Antonio2","232","jodar2")));


        List<Persona> personaListEsperada = new ArrayList<>();
        personaListEsperada.add((new Persona(1L,"Antonio1","23","jodar1")));
        personaListEsperada.add((new Persona(2L,"Antonio2","232","jodar2")));

        Mockito.when(personaService.listAllPersona()).thenReturn(personaListSimulada);
        final List<Persona> resultado = personaService.listAllPersona();
        assertEquals(personaListEsperada,resultado);
        Mockito.verify(personaRepository).findAll();

    }
}